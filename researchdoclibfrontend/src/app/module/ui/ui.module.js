"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var spinner_component_1 = require("./spinner/spinner.component");
var card_component_1 = require("./card/card.component");
var form_component_1 = require("./ui-form/form.component");
var drop_down_menu_component_1 = require("./drop-down-menu/drop-down-menu.component");
var ng_lightning_1 = require("ng-lightning");
var datatable_component_1 = require("./ui-datatable/datatable.component");
var UiModule = (function () {
    function UiModule() {
    }
    return UiModule;
}());
UiModule = __decorate([
    core_1.NgModule({
        imports: [
            common_1.CommonModule,
            forms_1.FormsModule,
            ng_lightning_1.NglModule
        ],
        exports: [
            spinner_component_1.SpinnerComponent,
            card_component_1.CardComponent,
            form_component_1.FormComponent,
            drop_down_menu_component_1.DropDownMenuComponent,
            datatable_component_1.DataTableComponent
        ],
        declarations: [
            spinner_component_1.SpinnerComponent,
            card_component_1.CardComponent,
            form_component_1.FormComponent,
            drop_down_menu_component_1.DropDownMenuComponent,
            datatable_component_1.DataTableComponent
        ],
        providers: []
    })
], UiModule);
exports.UiModule = UiModule;
