import { Directive, ElementRef, AfterViewInit } from '@angular/core';

/**
 * Apply on input fields to be auto focused.
 */
@Directive({
  selector: '[myAutoFocus]'
})
export class AutoFocusDirective implements AfterViewInit {

  constructor(private elementRef: ElementRef) {
  }

  ngAfterViewInit(): void {
    this.elementRef.nativeElement.focus();
  }
}
