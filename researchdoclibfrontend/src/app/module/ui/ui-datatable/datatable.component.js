"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var DataTableComponent = (function () {
    function DataTableComponent() {
        this.emitter = new core_1.EventEmitter();
    }
    DataTableComponent.prototype.onSort = function ($event) {
        var key = $event.key, order = $event.order;
        this.data.sort(function (a, b) {
            return (key === 'id' ? b[key] - a[key] : b[key].localeCompare(a[key])) * (order === 'desc' ? 1 : -1);
        });
    };
    DataTableComponent.prototype.onRowClick = function ($event) {
        this.emitter.emit($event);
    };
    return DataTableComponent;
}());
__decorate([
    core_1.Input()
], DataTableComponent.prototype, "data", void 0);
__decorate([
    core_1.Output()
], DataTableComponent.prototype, "emitter", void 0);
DataTableComponent = __decorate([
    core_1.Component({
        selector: 'ui-datatable',
        templateUrl: 'datatable.component.html'
    })
], DataTableComponent);
exports.DataTableComponent = DataTableComponent;
