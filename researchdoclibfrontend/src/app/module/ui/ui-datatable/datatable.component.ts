import { Component, EventEmitter, Input, Output } from '@angular/core';
import { INglDatatableRowClick, INglDatatableSort } from 'ng-lightning';

@Component({
    selector: 'ui-datatable',
    templateUrl: 'datatable.component.html'
})
export class DataTableComponent {

    @Input() data;
    @Output() emitter: EventEmitter<any> = new EventEmitter();

    constructor() {
    }

    onSort($event: INglDatatableSort) {
        const {key, order} = $event;
        this.data.sort((a: any, b: any) => {
            return (key === 'id' ? b[key] - a[key] : b[key].localeCompare(a[key])) * (order === 'desc' ? 1 : -1);
        });
    }

    onRowClick($event: INglDatatableRowClick) {
        this.emitter.emit($event);
    }
}
