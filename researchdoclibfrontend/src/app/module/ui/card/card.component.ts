import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'ui-card',
    templateUrl: 'card.component.html'
})
export class CardComponent {
    @Input() title;
    @Input() footer;

    constructor() {
    }
}
