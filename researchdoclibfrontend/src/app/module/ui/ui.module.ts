import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SpinnerComponent } from './spinner/spinner.component';
import { CardComponent } from './card/card.component';
import { FormComponent } from './ui-form/form.component';
import { DropDownMenuComponent } from './drop-down-menu/drop-down-menu.component';
import { NglModule } from 'ng-lightning';
import { DataTableComponent } from './ui-datatable/datatable.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NglModule
    ],
    exports: [
        SpinnerComponent,
        CardComponent,
        FormComponent,
        DropDownMenuComponent,
        DataTableComponent
    ],
    declarations: [
        SpinnerComponent,
        CardComponent,
        FormComponent,
        DropDownMenuComponent,
        DataTableComponent
    ],
    providers: []
})
export class UiModule {
}
