"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var TableRowSelectionComponent = (function () {
    function TableRowSelectionComponent() {
        /**
         * Defines the selected items by attribute.
         * @type {Array} list of selected items's attributes
         */
        this.selectedItems = [];
        /**
         * Event emitter to notify table about selection changes.
         * @type {EventEmitter}
         */
        this.selectedItemsChange = new core_1.EventEmitter();
        /**
         * State of all selection checkbox.
         * @type {boolean} false by default
         */
        this.allSelected = false;
    }
    /**
     *
     * @param id
     * @returns {boolean} is the checkbox selected
     */
    TableRowSelectionComponent.prototype.isSelected = function (id) {
        return !!this.selectedItems.find(function (selectedItem) { return selectedItem === id; });
    };
    TableRowSelectionComponent.prototype.trackBySelectionAttribute = function (index, item) {
        return item ? item[this.selectionAttribute] : undefined;
    };
    /**
     * Event for checkbox click.
     * @param checkbox checkbox clicked
     * @param id identifier or row checked
     */
    TableRowSelectionComponent.prototype.onItemClick = function (checkbox, id) {
        if (checkbox.target.checked) {
            // add checked row if not included in the array
            if (this.selectedItems.indexOf(id) < 0) {
                this.selectedItems.push(id);
            }
        }
        else {
            this.allSelected = false;
            // re,pve checked row if included in the array
            var indexOfDeselectedItem = this.selectedItems.indexOf(id);
            if (indexOfDeselectedItem >= 0) {
                this.selectedItems.splice(indexOfDeselectedItem, 1);
            }
        }
        // set state of select all checkbox
        this.initSelectAllFunction(this.items);
        this.selectedItemsChange.emit(this.selectedItems);
    };
    /**
     * Event handler of selectAll checkbox.
     * @param selectAllCheckbox
     */
    TableRowSelectionComponent.prototype.onSelectAll = function (selectAllCheckbox) {
        var _this = this;
        if (selectAllCheckbox.target.checked) {
            // select all (add the identifier of each row to selectedItems)
            this.items.forEach(function (item) {
                var selectionId = item[_this.selectionAttribute], indexOfDeselectedItem = _this.selectedItems.indexOf(selectionId);
                if (indexOfDeselectedItem < 0) {
                    _this.selectedItems.push(selectionId);
                }
            });
            this.selectedItemsChange.emit(this.selectedItems);
        }
        else {
            // deselect all (remove all identifier from selectedItems)
            this.items.forEach(function (item) {
                var selectionId = item[_this.selectionAttribute], indexOfDeselectedItem = _this.selectedItems.indexOf(selectionId);
                if (indexOfDeselectedItem >= 0) {
                    _this.selectedItems.splice(indexOfDeselectedItem, 1);
                }
            });
            this.selectedItemsChange.emit(this.selectedItems);
        }
    };
    /**
     * Set the state of selectAll.
     * @param items
     */
    TableRowSelectionComponent.prototype.initSelectAllFunction = function (items) {
        var _this = this;
        this.allSelected = items.every(function (item) {
            return _this.selectedItems.indexOf(item[_this.selectionAttribute]) >= 0;
        });
    };
    return TableRowSelectionComponent;
}());
__decorate([
    core_1.Input()
], TableRowSelectionComponent.prototype, "items", void 0);
__decorate([
    core_1.Input()
], TableRowSelectionComponent.prototype, "selectionAttribute", void 0);
__decorate([
    core_1.Input()
], TableRowSelectionComponent.prototype, "selectedItems", void 0);
__decorate([
    core_1.Output()
], TableRowSelectionComponent.prototype, "selectedItemsChange", void 0);
TableRowSelectionComponent = __decorate([
    core_1.Component({
        selector: 'ui-table-row-selection',
        templateUrl: 'table-row-selection.component.html',
        styleUrls: ['table-row-selection.component.scss']
    })
], TableRowSelectionComponent);
exports.TableRowSelectionComponent = TableRowSelectionComponent;
