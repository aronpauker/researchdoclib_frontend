import { Component, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'ui-table-row-selection',
  templateUrl: 'table-row-selection.component.html',
  styleUrls: ['table-row-selection.component.scss']
})
export class TableRowSelectionComponent {

  /**
   * Defines items on current page to generate selection for those.
   */
  @Input() items: any[];

  /**
   * Defines the selection attribute that the component has to collect.
   */
  @Input() selectionAttribute: any;

  /**
   * Defines the selected items by attribute.
   * @type {Array} list of selected items's attributes
   */
  @Input() selectedItems: any[] = [];

  /**
   * Event emitter to notify table about selection changes.
   * @type {EventEmitter}
   */
  @Output() selectedItemsChange: EventEmitter<any[]> = new EventEmitter();

  /**
   * State of all selection checkbox.
   * @type {boolean} false by default
   */
  allSelected = false;

  constructor() {
  }

  /**
   *
   * @param id
   * @returns {boolean} is the checkbox selected
   */
  isSelected(id): boolean {
    return !!this.selectedItems.find(selectedItem => selectedItem === id);
  }

  trackBySelectionAttribute(item) {
    return item ? item[this.selectionAttribute] : undefined;
  }

  /**
   * Event for checkbox click.
   * @param checkbox checkbox clicked
   * @param id identifier or row checked
   */
  onItemClick(checkbox, id) {
    if (checkbox.target.checked) {
      // add checked row if not included in the array
      if (this.selectedItems.indexOf(id) < 0) {
        this.selectedItems.push(id);
      }
    } else {
      this.allSelected = false;
      // re,pve checked row if included in the array
      let indexOfDeselectedItem = this.selectedItems.indexOf(id);
      if (indexOfDeselectedItem >= 0) {
        this.selectedItems.splice(indexOfDeselectedItem, 1);
      }
    }
    // set state of select all checkbox
    this.initSelectAllFunction(this.items);
    this.selectedItemsChange.emit(this.selectedItems);
  }

  /**
   * Event handler of selectAll checkbox.
   * @param selectAllCheckbox
   */
  onSelectAll(selectAllCheckbox) {
    if (selectAllCheckbox.target.checked) {
      // select all (add the identifier of each row to selectedItems)
        this.items.forEach(item => {
        let selectionId = item[this.selectionAttribute],
          indexOfDeselectedItem = this.selectedItems.indexOf(selectionId);
        if (indexOfDeselectedItem < 0) {
          this.selectedItems.push(selectionId);
        }
      });
      this.selectedItemsChange.emit(this.selectedItems);
    } else {
      // deselect all (remove all identifier from selectedItems)
      this.items.forEach(item => {
        let selectionId = item[this.selectionAttribute],
          indexOfDeselectedItem = this.selectedItems.indexOf(selectionId);
        if (indexOfDeselectedItem >= 0) {
          this.selectedItems.splice(indexOfDeselectedItem, 1);
        }
      });
      this.selectedItemsChange.emit(this.selectedItems);
    }
  }

  /**
   * Set the state of selectAll.
   * @param items
   */
  public initSelectAllFunction(items) {
    this.allSelected = items.every(item => {
      return this.selectedItems.indexOf(item[this.selectionAttribute]) >= 0;
    });
  }
}
