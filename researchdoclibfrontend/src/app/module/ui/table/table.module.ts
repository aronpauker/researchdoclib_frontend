import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NglModule } from 'ng-lightning';
import { TableComponent } from './table/table.component';
import { TableColComponent } from './table-col/table-col.component';
import { TableRowComponent } from './table-row/table-row.component';
import { TableHeaderComponent } from './table-header/table-header.component';
import { TableHeaderGroupComponent } from './table-header-group/table-header-group.component';
import { TableHeaderSeparatorComponent } from './table-header-separator/table-header-separator.component';
import { TableBodyComponent } from './table-body/table-body.component';
import { TableRowContextMenuComponent } from './table-row-context-menu/table-row-context-menu.component';
import { TableFooterComponent } from './table-footer/table-footer.component';
import { TableOverflowComponent } from './table-overflow/table-overflow.component';
import { TablePaginationComponent } from './table-pagination/table-pagination.component';
import { TableDecoratorComponent } from './table-decorator/table-decorator.component';
import { TableRowSelectionComponent } from './table-row-selection/table-row-selection.component';
import { TableRowActionComponent } from './table-row-action/table-row-action.component';
import { TableRowContextMenuOptionComponent } from './table-row-context-menu-option/table-row-context-menu-option.component';
import { AutoFocusDirective } from '../directive/AutoFocusDirective';

const UI_COMPONENTS = [
  TableComponent,
  TableColComponent,
  TableRowComponent,
  TableRowContextMenuComponent,
  TableRowContextMenuOptionComponent,
  TableBodyComponent,
  TableHeaderComponent,
  TableHeaderGroupComponent,
  TableHeaderSeparatorComponent,
  TableFooterComponent,
  TableOverflowComponent,
  TablePaginationComponent,
  TableDecoratorComponent,
  TableRowSelectionComponent,
  TableRowActionComponent
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NglModule
  ],
  declarations: [
    TableComponent,
    TableColComponent,
    TableRowComponent,
    TableRowContextMenuComponent,
    TableRowContextMenuOptionComponent,
    TableBodyComponent,
    TableHeaderComponent,
    TableHeaderGroupComponent,
    TableHeaderSeparatorComponent,
    TableFooterComponent,
    TableOverflowComponent,
    TablePaginationComponent,
    TableDecoratorComponent,
    TableRowSelectionComponent,
    TableRowActionComponent,

    AutoFocusDirective
  ],
  exports: UI_COMPONENTS
})
export class TableModule {

}
