import { Component, Input } from '@angular/core';

/**
 * Template and design purpose.
 */
@Component({
  selector: 'ui-table-row',
  templateUrl: 'table-row.component.html',
  styleUrls: ['table-row.component.scss']
})
export class TableRowComponent {

  constructor() {
  }

}
