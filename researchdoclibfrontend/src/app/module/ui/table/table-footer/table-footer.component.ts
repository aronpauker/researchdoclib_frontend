import { Component } from '@angular/core';

/**
 * Template and design purpose.
 */
@Component({
  selector: 'ui-table-footer',
  templateUrl: 'table-footer.component.html',
  styleUrls: ['table-footer.component.scss']
})
export class TableFooterComponent {

  constructor() {
  }

}
