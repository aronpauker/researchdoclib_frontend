import { Component } from '@angular/core';

/**
 * Template and design purpose.
 */
@Component({
  selector: 'ui-table-header-group',
  templateUrl: 'table-header-group.component.html',
  styleUrls: ['table-header-group.component.scss']
})
export class TableHeaderGroupComponent {

  constructor() {
  }

}
