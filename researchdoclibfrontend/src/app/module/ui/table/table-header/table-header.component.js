"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
/**
 * Defines a header for column in the table component.
 */
var TableHeaderComponent = (function () {
    function TableHeaderComponent() {
        /**
         * Column label to display.
         * @type {string} 'Unnamed' is the default label.
         */
        this.label = 'Unnamed';
        /**
         * Column width in pixels.
         * @type {number} 200 pixel is the default width value.
         */
        this.width = 200;
        /**
         * EventEmitter to notify the table about the column width changes.
         * @type {EventEmitter}
         */
        this.widthChange = new core_1.EventEmitter();
        /**
         * Option to make the column sortable.
         * @type {boolean} false by default
         */
        this.sortable = false;
        /**
         * Option to make the column resizable.
         * @type {boolean} false by default
         */
        this.resizable = false;
        /**
         * State of the header whether the table is sorted by that.
         * @type {boolean} false by default
         */
        this.sorted = false;
        /**
         * State of the sorting whether the table is descendingly sorted by the given header.
         * @type {boolean} false by default
         */
        this.descending = false;
        /**
         * Event emitter to notify the table about sorting requests.
         * @type {EventEmitter}
         */
        this.pageRequestChange = new core_1.EventEmitter();
    }
    Object.defineProperty(TableHeaderComponent.prototype, "isSorted", {
        /**
         * Provides the state of the column via auto change detection.
         * @returns {boolean}
         */
        get: function () {
            this.sorted = this.pageRequest.property === this.name;
            this.descending = this.sorted ? (this.pageRequest.direction === 'DESC' ? true : false) : false;
            return this.sorted;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Event handler to notify the table about column width changes.
     * @param columnWidth the new column width
     */
    TableHeaderComponent.prototype.onWidthChange = function (columnWidth) {
        this.widthChange.emit(columnWidth);
    };
    /**
     * Event handler to notify the table about sorting request.
     */
    TableHeaderComponent.prototype.onSort = function () {
        if (this.sortable) {
            this.pageRequest.direction = this.sorted ? (this.descending ? 'ASC' : 'DESC') : 'ASC';
            this.pageRequest.property = this.name;
            this.pageRequestChange.emit(this.pageRequest);
        }
    };
    return TableHeaderComponent;
}());
__decorate([
    core_1.Input()
], TableHeaderComponent.prototype, "name", void 0);
__decorate([
    core_1.Input()
], TableHeaderComponent.prototype, "label", void 0);
__decorate([
    core_1.Input()
], TableHeaderComponent.prototype, "width", void 0);
__decorate([
    core_1.Output()
], TableHeaderComponent.prototype, "widthChange", void 0);
__decorate([
    core_1.Input()
], TableHeaderComponent.prototype, "sortable", void 0);
__decorate([
    core_1.Input()
], TableHeaderComponent.prototype, "resizable", void 0);
__decorate([
    core_1.Input()
], TableHeaderComponent.prototype, "pageRequest", void 0);
__decorate([
    core_1.Output()
], TableHeaderComponent.prototype, "pageRequestChange", void 0);
TableHeaderComponent = __decorate([
    core_1.Component({
        selector: 'ui-table-header',
        templateUrl: 'table-header.component.html',
        styleUrls: ['table-header.component.scss']
    })
], TableHeaderComponent);
exports.TableHeaderComponent = TableHeaderComponent;
