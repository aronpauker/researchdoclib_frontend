import { Component, Input, Output, EventEmitter } from '@angular/core';
import { PageRequest } from '../object/PageRequest';

/**
 * Defines a header for column in the table component.
 */
@Component({
  selector: 'ui-table-header',
  templateUrl: 'table-header.component.html',
  styleUrls: ['table-header.component.scss']
})
export class TableHeaderComponent {

  /**
   * Name of JSON property.
   */
  @Input() name: string;

  /**
   * Column label to display.
   * @type {string} 'Unnamed' is the default label.
   */
  @Input() label = 'Unnamed';

  /**
   * Column width in pixels.
   * @type {number} 200 pixel is the default width value.
   */
  @Input() width = 200;

  /**
   * EventEmitter to notify the table about the column width changes.
   * @type {EventEmitter}
   */
  @Output() widthChange: EventEmitter<number> = new EventEmitter();

  /**
   * Option to make the column sortable.
   * @type {boolean} false by default
   */
  @Input() sortable = false;

  /**
   * Option to make the column resizable.
   * @type {boolean} false by default
   */
  @Input() resizable = false;

  /**
   * State of the header whether the table is sorted by that.
   * @type {boolean} false by default
   */
  sorted = false;

  /**
   * State of the sorting whether the table is descendingly sorted by the given header.
   * @type {boolean} false by default
   */
  descending = false;

  /**
   * The header component can decide its own state based on the request object.
   */
  @Input() pageRequest: PageRequest;

  /**
   * Event emitter to notify the table about sorting requests.
   * @type {EventEmitter}
   */
  @Output() pageRequestChange: EventEmitter<PageRequest> = new EventEmitter();

  constructor() {
  }

  /**
   * Provides the state of the column via auto change detection.
   * @returns {boolean}
   */
  get isSorted() {
    this.sorted = this.pageRequest.property === this.name;
    this.descending = this.sorted ? (this.pageRequest.direction === 'DESC' ? true : false) : false;
    return this.sorted;
  }

  /**
   * Event handler to notify the table about column width changes.
   * @param columnWidth the new column width
   */
  onWidthChange(columnWidth) {
    this.widthChange.emit(columnWidth);
  }

  /**
   * Event handler to notify the table about sorting request.
   */
  onSort() {
    if (this.sortable) {
      this.pageRequest.direction = this.sorted ? (this.descending ? 'ASC' : 'DESC') : 'ASC';
      this.pageRequest.property = this.name;
      this.pageRequestChange.emit(this.pageRequest);
    }
  }

}
