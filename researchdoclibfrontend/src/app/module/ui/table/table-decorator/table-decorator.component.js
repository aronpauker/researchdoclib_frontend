"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
/**
 * It has design and column visibility selection purpose.
 */
var TableDecoratorComponent = (function () {
    function TableDecoratorComponent() {
        /**
         * Define the selected columns.
         * @type {Array} list of selected columns
         */
        this.selectedColumns = [];
        /**
         * Send notification of changed column visibility options.
         * @type {EventEmitter}
         */
        this.selectedColumnsChange = new core_1.EventEmitter();
        /**
         * Text content of column selection component when no column is selected.
         * @type {string}
         */
        this.noColumnSelectedText = 'Select column(s)';
        /**
         * Text content of column selection component to display the cound of selected columns.
         * @type {string}
         */
        this.columnsSelectedText = ' column(s) selected';
        /**
         * Display a switch to optionally show/hide certain columns.
         * @type {boolean} false by default
         */
        this.optionalColumnSwitch = false;
        /**
         * State of the column selection pickList component.
         * @type {boolean} false by default
         */
        this.open = false;
    }
    TableDecoratorComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Collects the column to be optionally show/hide (filters the default column).
        this.nonDefaultColumns = [];
        this.columns.forEach(function (column) {
            if (!column.isDefault) {
                _this.nonDefaultColumns.push(column);
            }
            if (column.visible) {
                _this.selectedColumns.push(column);
            }
        });
        this.selectedColumnsChange.emit(this.selectedColumns);
    };
    Object.defineProperty(TableDecoratorComponent.prototype, "paginationInfo", {
        /**
         * Provides pagination information to the footer,
         * that represents the range of fields showed and the total count.
         * Eg. 51-60 of 2312
         * @returns {string}
         */
        get: function () {
            var nonDefaultColumnsLength = this.selectedColumns.filter(function (selectedColumn) { return selectedColumn.isDefault === false; }).length;
            return this.selectedColumns && nonDefaultColumnsLength ?
                (nonDefaultColumnsLength + this.columnsSelectedText) :
                this.noColumnSelectedText;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Emits the selected column to the listeners when the switcher get closed.
     * @param opened state of the column picklist
     */
    TableDecoratorComponent.prototype.onSelectedColumnsChange = function (opened) {
        if (!opened) {
            this.selectedColumnsChange.emit(this.selectedColumns);
        }
    };
    return TableDecoratorComponent;
}());
__decorate([
    core_1.Input()
], TableDecoratorComponent.prototype, "title", void 0);
__decorate([
    core_1.Input()
], TableDecoratorComponent.prototype, "columns", void 0);
__decorate([
    core_1.Input()
], TableDecoratorComponent.prototype, "selectedColumns", void 0);
__decorate([
    core_1.Output()
], TableDecoratorComponent.prototype, "selectedColumnsChange", void 0);
__decorate([
    core_1.Input()
], TableDecoratorComponent.prototype, "noColumnSelectedText", void 0);
__decorate([
    core_1.Input()
], TableDecoratorComponent.prototype, "columnsSelectedText", void 0);
__decorate([
    core_1.Input()
], TableDecoratorComponent.prototype, "optionalColumnSwitch", void 0);
TableDecoratorComponent = __decorate([
    core_1.Component({
        selector: 'ui-table-decorator',
        templateUrl: 'table-decorator.component.html',
        styleUrls: ['table-decorator.component.scss']
    })
], TableDecoratorComponent);
exports.TableDecoratorComponent = TableDecoratorComponent;
