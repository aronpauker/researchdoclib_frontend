import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

/**
 * It has design and column visibility selection purpose.
 */
@Component({
  selector: 'ui-table-decorator',
  templateUrl: 'table-decorator.component.html',
  styleUrls: ['table-decorator.component.scss']
})
export class TableDecoratorComponent implements OnInit {

  /**
   * Title of the table.
   */
  @Input() title: string;

  /**
   * Define columns to optionally display/hide them.
   */
  @Input() columns: any[];

  /**
   * Filtered columns list that doesn't contain the default columns.
   */
  nonDefaultColumns: any[];

  /**
   * Define the selected columns.
   * @type {Array} list of selected columns
   */
  @Input() selectedColumns: any = [];

  /**
   * Send notification of changed column visibility options.
   * @type {EventEmitter}
   */
  @Output() selectedColumnsChange: EventEmitter<any[]> = new EventEmitter();

  /**
   * Text content of column selection component when no column is selected.
   * @type {string}
   */
  @Input() noColumnSelectedText = 'Select column(s)';

  /**
   * Text content of column selection component to display the cound of selected columns.
   * @type {string}
   */
  @Input() columnsSelectedText = ' column(s) selected';

  /**
   * Display a switch to optionally show/hide certain columns.
   * @type {boolean} false by default
   */
  @Input() optionalColumnSwitch = false;

  /**
   * State of the column selection pickList component.
   * @type {boolean} false by default
   */
  open = false;


  constructor() {
  }

  ngOnInit() {
    // Collects the column to be optionally show/hide (filters the default column).
    this.nonDefaultColumns = [];
    this.columns.forEach(column => {
      if (!column.isDefault) {
        this.nonDefaultColumns.push(column);
      }
      if (column.visible) {
        this.selectedColumns.push(column);
      }
    });
    this.selectedColumnsChange.emit(this.selectedColumns);
  }

  /**
   * Provides pagination information to the footer,
   * that represents the range of fields showed and the total count.
   * Eg. 51-60 of 2312
   * @returns {string}
   */
  get paginationInfo() {
    let nonDefaultColumnsLength = this.selectedColumns.filter(selectedColumn => selectedColumn.isDefault === false).length;
    return this.selectedColumns && nonDefaultColumnsLength ?
      (nonDefaultColumnsLength + this.columnsSelectedText) :
      this.noColumnSelectedText;
  }

  /**
   * Emits the selected column to the listeners when the switcher get closed.
   * @param opened state of the column picklist
   */
  onSelectedColumnsChange(opened) {
    if (!opened) {
      this.selectedColumnsChange.emit(this.selectedColumns);
    }
  }

}
