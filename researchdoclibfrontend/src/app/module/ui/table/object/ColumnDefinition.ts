import { isUndefined } from 'util';
/**
 * Defines the attributes of table columns.
 *
 * @param name JSON property name
 * @param label title of the column header
 * @param width column's width
 * @param visible visibility
 * @param isDefault is the visibility changeable
 * @param editable is the column editable
 * @param resizable is the column resizable
 * @param sortable is the column sortable
 */
export class ColumnDefinition {

  constructor(private name: string,
              private label: string,
              private width?: number,
              private visible?: boolean,
              private isDefault?: boolean,
              private editable?: boolean,
              private resizable?: boolean,
              private sortable?: boolean) {

    // set default values for attributes
    width = isUndefined(width) ? 200 : width;
    visible = isUndefined(visible) ? true : visible;
    isDefault = isUndefined(isDefault) ? false : isDefault;
    editable = isUndefined(editable) ? false : editable;
    resizable = isUndefined(resizable) ? false : resizable;
    sortable = isUndefined(sortable) ? false : sortable;

  }

}
