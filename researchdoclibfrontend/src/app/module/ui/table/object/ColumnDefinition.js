"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var util_1 = require("util");
/**
 * Defines the attributes of table columns.
 *
 * @param name JSON property name
 * @param label title of the column header
 * @param width column's width
 * @param visible visibility
 * @param isDefault is the visibility changeable
 * @param editable is the column editable
 * @param resizable is the column resizable
 * @param sortable is the column sortable
 */
var ColumnDefinition = (function () {
    function ColumnDefinition(name, label, width, visible, isDefault, editable, resizable, sortable) {
        this.name = name;
        this.label = label;
        this.width = width;
        this.visible = visible;
        this.isDefault = isDefault;
        this.editable = editable;
        this.resizable = resizable;
        this.sortable = sortable;
        // set default values for attributes
        width = util_1.isUndefined(width) ? 200 : width;
        visible = util_1.isUndefined(visible) ? true : visible;
        isDefault = util_1.isUndefined(isDefault) ? false : isDefault;
        editable = util_1.isUndefined(editable) ? false : editable;
        resizable = util_1.isUndefined(resizable) ? false : resizable;
        sortable = util_1.isUndefined(sortable) ? false : sortable;
    }
    return ColumnDefinition;
}());
exports.ColumnDefinition = ColumnDefinition;
