"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * TODO - experimental version
 * Defines the attributes of context menu item.
 *
 * @param label name of context menu item
 * @param action - javascript function to call
 * @param url router parameter to redirect
 */
var ContextMenu = (function () {
    function ContextMenu(label, action, url) {
        this.label = label;
        this.action = action;
        this.url = url;
    }
    return ContextMenu;
}());
exports.ContextMenu = ContextMenu;
