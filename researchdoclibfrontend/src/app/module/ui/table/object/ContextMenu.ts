/**
 * TODO - experimental version
 * Defines the attributes of context menu item.
 *
 * @param label name of context menu item
 * @param action - javascript function to call
 * @param url router parameter to redirect
 */
export class ContextMenu {

  constructor(private label: string,
              private action: any,
              private url: string) {

  }
}
