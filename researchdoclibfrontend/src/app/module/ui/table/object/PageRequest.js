"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Aligned to Spring's PageRequest class.
 *
 * @param page - page number
 * @param size - items' count on a page
 * @param direction - sorting direction
 * @param property - sorting by column name
 */
var PageRequest = (function () {
    function PageRequest(page, size, property, direction) {
        this.page = page;
        this.size = size;
        this.property = property;
        this.direction = direction;
    }
    return PageRequest;
}());
exports.PageRequest = PageRequest;
