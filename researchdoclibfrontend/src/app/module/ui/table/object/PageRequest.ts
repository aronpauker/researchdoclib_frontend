/**
 * Aligned to Spring's PageRequest class.
 *
 * @param page - page number
 * @param size - items' count on a page
 * @param direction - sorting direction
 * @param property - sorting by column name
 */
export class PageRequest {

  constructor(public page?: number,
              public size?: number,
              public property?: string,
              public direction?: string) {
  }

}
