"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
/**
 * Item inside a context menu.
 */
var TableRowContextMenuOptionComponent = (function () {
    function TableRowContextMenuOptionComponent() {
    }
    return TableRowContextMenuOptionComponent;
}());
__decorate([
    core_1.Input()
], TableRowContextMenuOptionComponent.prototype, "label", void 0);
TableRowContextMenuOptionComponent = __decorate([
    core_1.Component({
        selector: 'ui-table-row-context-menu-option',
        templateUrl: 'table-row-context-menu-option.component.html',
        styleUrls: ['table-row-context-menu-option.component.scss']
    })
], TableRowContextMenuOptionComponent);
exports.TableRowContextMenuOptionComponent = TableRowContextMenuOptionComponent;
