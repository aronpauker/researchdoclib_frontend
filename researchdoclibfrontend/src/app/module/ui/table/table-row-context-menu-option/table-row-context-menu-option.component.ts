import { Component, Input } from '@angular/core';

/**
 * Item inside a context menu.
 */
@Component({
  selector: 'ui-table-row-context-menu-option',
  templateUrl: 'table-row-context-menu-option.component.html',
  styleUrls: ['table-row-context-menu-option.component.scss']
})
export class TableRowContextMenuOptionComponent {

  /**
   * Define the label of context menu item.
   */
  @Input() label: string;

  constructor() {

  }
}
