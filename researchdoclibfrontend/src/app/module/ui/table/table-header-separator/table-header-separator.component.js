"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var MIN_WIDTH = 30;
/**
 * Table column resizing purpose.
 */
var TableHeaderSeparatorComponent = (function () {
    function TableHeaderSeparatorComponent(elementRef) {
        this.elementRef = elementRef;
        /**
         * Event emitter to notify the listeners about width changes.
         * @type {EventEmitter}
         */
        this.widthChange = new core_1.EventEmitter();
        /**
         * State of the mouse event.
         * @type {boolean}
         */
        this.isDragging = false;
    }
    /**
     * Set the dragging state and the color of the separator.
     * @param $event mouse event to prevent bubbling effect.
     */
    TableHeaderSeparatorComponent.prototype.onMouseDown = function ($event) {
        $event.preventDefault();
        this.isDragging = true;
        this.elementRef.nativeElement.classList.add('dragging');
    };
    /**
     * Leave dragging state and remove the separator's visibility.
     */
    TableHeaderSeparatorComponent.prototype.onMouseUp = function () {
        this.isDragging = false;
        this.elementRef.nativeElement.classList.remove('dragging');
    };
    /**
     * Dragging event handler.
     * @param $event
     */
    TableHeaderSeparatorComponent.prototype.onMouseMove = function ($event) {
        if (this.isDragging) {
            // calculate the left position of separator
            // left position of the mouse on the screen - parent's left position on the screen
            var left = $event.pageX - this.elementRef.nativeElement.parentElement.getBoundingClientRect().left;
            left = left > MIN_WIDTH ? left : MIN_WIDTH;
            // adjust with the width of the separator
            this.width = left + 2;
            this.widthChange.emit(left);
        }
    };
    return TableHeaderSeparatorComponent;
}());
__decorate([
    core_1.Input()
], TableHeaderSeparatorComponent.prototype, "width", void 0);
__decorate([
    core_1.Output()
], TableHeaderSeparatorComponent.prototype, "widthChange", void 0);
__decorate([
    core_1.HostListener('mousedown', ['$event'])
], TableHeaderSeparatorComponent.prototype, "onMouseDown", null);
__decorate([
    core_1.HostListener('window:mouseup')
], TableHeaderSeparatorComponent.prototype, "onMouseUp", null);
__decorate([
    core_1.HostListener('body:mousemove', ['$event'])
], TableHeaderSeparatorComponent.prototype, "onMouseMove", null);
TableHeaderSeparatorComponent = __decorate([
    core_1.Component({
        selector: 'ui-table-header-separator',
        templateUrl: 'table-header-separator.component.html',
        styleUrls: ['table-header-separator.component.scss']
    })
], TableHeaderSeparatorComponent);
exports.TableHeaderSeparatorComponent = TableHeaderSeparatorComponent;
