import { Component, ElementRef, HostListener, Output, EventEmitter, Input } from '@angular/core';

const MIN_WIDTH = 30;

/**
 * Table column resizing purpose.
 */
@Component({
  selector: 'ui-table-header-separator',
  templateUrl: 'table-header-separator.component.html',
  styleUrls: ['table-header-separator.component.scss']
})
export class TableHeaderSeparatorComponent {

  /**
   * Default width value that sets the left position of separator.
   */
  @Input() width: number;

  /**
   * Event emitter to notify the listeners about width changes.
   * @type {EventEmitter}
   */
  @Output() widthChange: EventEmitter<number> = new EventEmitter();

  /**
   * State of the mouse event.
   * @type {boolean}
   */
  isDragging = false;

  /**
   * Set the dragging state and the color of the separator.
   * @param $event mouse event to prevent bubbling effect.
   */
  @HostListener('mousedown', ['$event']) onMouseDown($event: any) {
    $event.preventDefault();
    this.isDragging = true;
    this.elementRef.nativeElement.classList.add('dragging');
  }

  /**
   * Leave dragging state and remove the separator's visibility.
   */
  @HostListener('window:mouseup') onMouseUp() {
    this.isDragging = false;
    this.elementRef.nativeElement.classList.remove('dragging');
  }

  /**
   * Dragging event handler.
   * @param $event
   */
  @HostListener('body:mousemove', ['$event']) onMouseMove($event: any) {
    if (this.isDragging) {
      // calculate the left position of separator
      // left position of the mouse on the screen - parent's left position on the screen
      let left = $event.pageX - this.elementRef.nativeElement.parentElement.getBoundingClientRect().left;
      left = left > MIN_WIDTH ? left : MIN_WIDTH;
      // adjust with the width of the separator
      this.width = left + 2;
      this.widthChange.emit(left);
    }
  }

  constructor(private elementRef: ElementRef) {
  }

}
