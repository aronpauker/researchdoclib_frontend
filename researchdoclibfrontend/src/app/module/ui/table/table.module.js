"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var common_1 = require("@angular/common");
var ng_lightning_1 = require("ng-lightning");
var table_component_1 = require("./table/table.component");
var table_col_component_1 = require("./table-col/table-col.component");
var table_row_component_1 = require("./table-row/table-row.component");
var table_header_component_1 = require("./table-header/table-header.component");
var table_header_group_component_1 = require("./table-header-group/table-header-group.component");
var table_header_separator_component_1 = require("./table-header-separator/table-header-separator.component");
var table_body_component_1 = require("./table-body/table-body.component");
var table_row_context_menu_component_1 = require("./table-row-context-menu/table-row-context-menu.component");
var table_footer_component_1 = require("./table-footer/table-footer.component");
var table_overflow_component_1 = require("./table-overflow/table-overflow.component");
var table_pagination_component_1 = require("./table-pagination/table-pagination.component");
var table_decorator_component_1 = require("./table-decorator/table-decorator.component");
var table_row_selection_component_1 = require("./table-row-selection/table-row-selection.component");
var table_row_action_component_1 = require("./table-row-action/table-row-action.component");
var table_row_context_menu_option_component_1 = require("./table-row-context-menu-option/table-row-context-menu-option.component");
var AutoFocusDirective_1 = require("../directive/AutoFocusDirective");
var UI_COMPONENTS = [
    table_component_1.TableComponent,
    table_col_component_1.TableColComponent,
    table_row_component_1.TableRowComponent,
    table_row_context_menu_component_1.TableRowContextMenuComponent,
    table_row_context_menu_option_component_1.TableRowContextMenuOptionComponent,
    table_body_component_1.TableBodyComponent,
    table_header_component_1.TableHeaderComponent,
    table_header_group_component_1.TableHeaderGroupComponent,
    table_header_separator_component_1.TableHeaderSeparatorComponent,
    table_footer_component_1.TableFooterComponent,
    table_overflow_component_1.TableOverflowComponent,
    table_pagination_component_1.TablePaginationComponent,
    table_decorator_component_1.TableDecoratorComponent,
    table_row_selection_component_1.TableRowSelectionComponent,
    table_row_action_component_1.TableRowActionComponent
];
var TableModule = (function () {
    function TableModule() {
    }
    return TableModule;
}());
TableModule = __decorate([
    core_1.NgModule({
        imports: [
            common_1.CommonModule,
            forms_1.FormsModule,
            ng_lightning_1.NglModule
        ],
        declarations: [
            table_component_1.TableComponent,
            table_col_component_1.TableColComponent,
            table_row_component_1.TableRowComponent,
            table_row_context_menu_component_1.TableRowContextMenuComponent,
            table_row_context_menu_option_component_1.TableRowContextMenuOptionComponent,
            table_body_component_1.TableBodyComponent,
            table_header_component_1.TableHeaderComponent,
            table_header_group_component_1.TableHeaderGroupComponent,
            table_header_separator_component_1.TableHeaderSeparatorComponent,
            table_footer_component_1.TableFooterComponent,
            table_overflow_component_1.TableOverflowComponent,
            table_pagination_component_1.TablePaginationComponent,
            table_decorator_component_1.TableDecoratorComponent,
            table_row_selection_component_1.TableRowSelectionComponent,
            table_row_action_component_1.TableRowActionComponent,
            AutoFocusDirective_1.AutoFocusDirective
        ],
        exports: UI_COMPONENTS
    })
], TableModule);
exports.TableModule = TableModule;
