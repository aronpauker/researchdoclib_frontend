import { Component, HostListener, Input, Output, EventEmitter, OnInit } from '@angular/core';

/**
 * TODO - Inline editing function is an experimental feature. Date type definition is missing. E.g. editing date fields.
 * Column component of table row.
 */
@Component({
  selector: 'ui-table-col',
  templateUrl: 'table-col.component.html',
  styleUrls: ['table-col.component.scss']
})
export class TableColComponent implements OnInit {

  /**
   * Option to make the column to be editable.
   * @type {boolean}
   */
  @Input() editable = false;

  /**
   * Provides an id to identify the modified entity during inline editing.
   */
  @Input() id: any;

  /**
   * Provides the column name to identify the modified property of the entity during inline editing.
   */
  @Input() name: string;

  /**
   * Provides the original value of the column to modify that.
   */
  @Input() value: any;

  /**
   * Provides event to edit
   * @type {EventEmitter}
   */
  @Output() edited: EventEmitter<any> = new EventEmitter();

  /**
   * State of the column.
   * @type {boolean} false by default
   */
  edit = false;

  /**
   * State of the column value.
   * It is initialized when the component initialized
   * and updated when onEdit() method fires.
   */
  originalValue: any;

  /**
   * Event binding for editing purpose.
   */
  @HostListener('click', ['$event']) onClick() {
    this.edit = true;
  }

  constructor() {
  }

  ngOnInit(): void {
    // Save the state of the initial value.
    this.originalValue = this.value;
  }

  /**
   * Emit the change event to the listeners.
   */
  onEdit() {
    this.edited.emit({id: this.id, name: this.name, value: this.value});
    this.originalValue = this.value;
    this.edit = false;
  }

  /**
   * Hide the inline editing input field when the focus is left.
   */
  onBlur() {
    this.edit = false;
    this.value = this.originalValue;
  }

}
