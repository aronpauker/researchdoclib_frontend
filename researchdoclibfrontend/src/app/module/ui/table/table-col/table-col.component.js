"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
/**
 * TODO - Inline editing function is an experimental feature. Date type definition is missing. E.g. editing date fields.
 * Column component of table row.
 */
var TableColComponent = (function () {
    function TableColComponent() {
        /**
         * Option to make the column to be editable.
         * @type {boolean}
         */
        this.editable = false;
        /**
         * Provides event to edit
         * @type {EventEmitter}
         */
        this.edited = new core_1.EventEmitter();
        /**
         * State of the column.
         * @type {boolean} false by default
         */
        this.edit = false;
    }
    /**
     * Event binding for editing purpose.
     */
    TableColComponent.prototype.onClick = function (event) {
        this.edit = true;
    };
    TableColComponent.prototype.ngOnInit = function () {
        // Save the state of the initial value.
        this.originalValue = this.value;
    };
    /**
     * Emit the change event to the listeners.
     */
    TableColComponent.prototype.onEdit = function () {
        this.edited.emit({ id: this.id, name: this.name, value: this.value });
        this.originalValue = this.value;
        this.edit = false;
    };
    /**
     * Hide the inline editing input field when the focus is left.
     */
    TableColComponent.prototype.onBlur = function () {
        this.edit = false;
        this.value = this.originalValue;
    };
    return TableColComponent;
}());
__decorate([
    core_1.Input()
], TableColComponent.prototype, "editable", void 0);
__decorate([
    core_1.Input()
], TableColComponent.prototype, "id", void 0);
__decorate([
    core_1.Input()
], TableColComponent.prototype, "name", void 0);
__decorate([
    core_1.Input()
], TableColComponent.prototype, "value", void 0);
__decorate([
    core_1.Output()
], TableColComponent.prototype, "edited", void 0);
__decorate([
    core_1.HostListener('click', ['$event'])
], TableColComponent.prototype, "onClick", null);
TableColComponent = __decorate([
    core_1.Component({
        selector: 'ui-table-col',
        templateUrl: 'table-col.component.html',
        styleUrls: ['table-col.component.scss']
    })
], TableColComponent);
exports.TableColComponent = TableColComponent;
