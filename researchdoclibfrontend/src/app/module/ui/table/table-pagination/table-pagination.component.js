"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
/**
 * Support pagination function for table component.
 */
var TablePaginationComponent = (function () {
    function TablePaginationComponent() {
        /**
         * Option to declare the page size options.
         * @type {[number,number,number,number,number]}
         */
        this.pageSizeOptions = [5, 10, 20, 30, 50];
        /**
         * Define page size label to display.
         * @type {string} 'Page size' is the default.
         */
        this.pageSizeLabel = 'Page size';
        /**
         * Define the next button's label to display.
         * @type {string} 'Next' is the default.
         */
        this.nextPageText = 'Next';
        /**
         * Define the previous button's label to display.
         * @type {string} 'Previous' is the default.
         */
        this.previousPageText = 'Previous';
        /**
         * Event emitter to notify the listener about page size changes.
         * @type {EventEmitter}
         */
        this.pageSizeChange = new core_1.EventEmitter();
        /**
         * Define the page number of pagination.
         * @type {number} 1 is the default
         */
        this.pageNumber = 1;
        /**
         * Event emitter to notify the listener about the selected page number.
         * @type {EventEmitter}
         */
        this.pageNumberChange = new core_1.EventEmitter();
        /**
         * Event emitter for totalElements change event.
         * @type {EventEmitter}
         */
        this.totalElementsChange = new core_1.EventEmitter();
    }
    TablePaginationComponent.prototype.ngOnInit = function () {
        this.lastPageSize = this.pageSize;
    };
    /**
     * Event handler for page size changes.
     * @param isOpened
     */
    TablePaginationComponent.prototype.onPageSizeChange = function (isOpened) {
        if (!isOpened && this.pageSize !== this.lastPageSize) {
            this.lastPageSize = this.pageSize;
            this.pageSizeChange.emit(this.pageSize);
        }
    };
    /**
     * Event handler for page number changes.
     * @param pageNumber
     */
    TablePaginationComponent.prototype.onPageNumberChange = function (pageNumber) {
        this.pageNumberChange.emit(pageNumber);
    };
    return TablePaginationComponent;
}());
__decorate([
    core_1.Input()
], TablePaginationComponent.prototype, "pageSizeOptions", void 0);
__decorate([
    core_1.Input()
], TablePaginationComponent.prototype, "pageSizeLabel", void 0);
__decorate([
    core_1.Input()
], TablePaginationComponent.prototype, "nextPageText", void 0);
__decorate([
    core_1.Input()
], TablePaginationComponent.prototype, "previousPageText", void 0);
__decorate([
    core_1.Input()
], TablePaginationComponent.prototype, "pageSize", void 0);
__decorate([
    core_1.Output()
], TablePaginationComponent.prototype, "pageSizeChange", void 0);
__decorate([
    core_1.Input()
], TablePaginationComponent.prototype, "pageNumber", void 0);
__decorate([
    core_1.Output()
], TablePaginationComponent.prototype, "pageNumberChange", void 0);
__decorate([
    core_1.Input()
], TablePaginationComponent.prototype, "totalElements", void 0);
__decorate([
    core_1.Output()
], TablePaginationComponent.prototype, "totalElementsChange", void 0);
TablePaginationComponent = __decorate([
    core_1.Component({
        selector: 'ui-table-pagination',
        templateUrl: 'table-pagination.component.html',
        styleUrls: ['table-pagination.component.scss']
    })
], TablePaginationComponent);
exports.TablePaginationComponent = TablePaginationComponent;
