import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

/**
 * Support pagination function for table component.
 */
@Component({
  selector: 'ui-table-pagination',
  templateUrl: 'table-pagination.component.html',
  styleUrls: ['table-pagination.component.scss']
})
export class TablePaginationComponent implements OnInit {

  /**
   * Option to declare the page size options.
   * @type {[number,number,number,number,number]}
   */
  @Input() pageSizeOptions = [5, 10, 20, 30, 50];

  /**
   * Define page size label to display.
   * @type {string} 'Page size' is the default.
   */
  @Input() pageSizeLabel = 'Page size';

  /**
   * Define the next button's label to display.
   * @type {string} 'Next' is the default.
   */
  @Input() nextPageText = 'Next';

  /**
   * Define the previous button's label to display.
   * @type {string} 'Previous' is the default.
   */
  @Input() previousPageText = 'Previous';

  /**
   * Define the page size.
   */
  @Input() pageSize: number;

  /**
   * Event emitter to notify the listener about page size changes.
   * @type {EventEmitter}
   */
  @Output() pageSizeChange: EventEmitter<number> = new EventEmitter();

  /**
   * Define the page number of pagination.
   * @type {number} 1 is the default
   */
  @Input() pageNumber = 1;

  /**
   * Event emitter to notify the listener about the selected page number.
   * @type {EventEmitter}
   */
  @Output() pageNumberChange: EventEmitter<number> = new EventEmitter();

  /**
   * Define the total elements to calculate the count of pages.
   */
  @Input() totalElements: number;

  /**
   * Event emitter for totalElements change event.
   * @type {EventEmitter}
   */
  @Output() totalElementsChange: EventEmitter<number> = new EventEmitter();

  /**
   * The value of page size selected.
   */
  lastPageSize: number;

  /**
   * Current page number
   */
  lastPageNumber: number;

  /**
   * The page size picklist status
   */
  open: boolean;

  constructor() {
  }

  ngOnInit() {
    this.lastPageSize = this.pageSize;
  }

  /**
   * Event handler for page size changes.
   * @param isOpened
   */
  onPageSizeChange(isOpened) {
    if (!isOpened && this.pageSize !== this.lastPageSize) {
      this.lastPageSize = this.pageSize;
      this.pageSizeChange.emit(this.pageSize);
    }
  }

  /**
   * Event handler for page number changes.
   * @param pageNumber
   */
  onPageNumberChange(pageNumber) {
    this.pageNumberChange.emit(pageNumber);
  }

}
