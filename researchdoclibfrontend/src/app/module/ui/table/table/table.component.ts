import { Component, Input } from '@angular/core';

/**
 * Template purpose.
 * Encapsulate the table's parts.
 */
@Component({
  selector: 'ui-table',
  templateUrl: 'table.component.html',
  styleUrls: ['table.component.scss']
})
export class TableComponent {

  /**
   * Option to hide table header.
   * @type {boolean} false by default
   */
  @Input() hideHeader = false;

  /**
   * Option to hide table footer.
   * @type {boolean} false by default
   */
  @Input() hideFooter = false;

  /**
   * Option to make the table functions inactive.
   * E.g. while being await for RESTful call
   * @type {boolean} false by default
   */
  @Input() loading = false;

  /**
   * Option for design purposes.
   * Affects the table body's width.
   * @type {boolean} false by default
   */
  @Input() multiSelectable = false;

  /**
   * Option for design purposes.
   * Affects the table body's width.
   * @type {boolean} false by default
   */
  @Input() hasRowAction = false;

  constructor() {
  }

}
