import { Component } from '@angular/core';

/**
 * @deprecated Move stile to table.
 * Overflow purpose.
 */
@Component({
  selector: 'ui-table-overflow',
  templateUrl: 'table-overflow.component.html',
  styleUrls: ['table-overflow.component.scss']
})
export class TableOverflowComponent {

  constructor() {
  }

}
