Angular2 table component
========================

Description.

List of sub components
======================

* **ui-table**encapsulates the table parts.
* **ui-table-decorator**shows the title of the table and a column switcher.
* **ui-table-header-group** 
* **ui-table-body** 
* **ui-table-footer** 
* **ui-table-header** 
* **ui-table-row** 
* **ui-table-col** 
* **ui-table-row-selection** 
* **ui-table-row-action** 
* **ui-table-row-context-menu** 
* **ui-table-pagination**





Table
================================================================

Table component has a header and footer decorator by default.

You can hide these by using input parameters.

The different parts of the table are optionally added by named transclusion. e.g. `table-header-group`

Example
----------------------------------------------------------------

```html
<ui-table
    [hideFooter]="true">
    
    <ui-table-header-group
        table-header-group
    >
        <!--HEADERS-->
    </ui-table-header-group>
    
    <!--OTHER TABLE PARTS-->
</ui-table>
```

API
----------------------------------------------------------------

> **Selector**
>
> * `<ui-table>`
>
> **Input**
>
> * `hideHeader?: boolean` Hide the header decorator.
> * `hideFooter?: boolean` Hide the footer decorator.
>  
> **Transclusion**
> * `table-decorator` Title of table and column selection option.
> * `table-row-selection` Multi selection feature.
> * `table-header-group` Group of column headers.
> * `table-body` Contains the rows of the table.
> * `table-row-action` Row action feature with context menu.
> * `table-footer` Paging feature. 





Decorator
================================================================

The decorator shows the**title**of the table and can hold a**column switcher**.

Example
----------------------------------------------------------------

```
<ui-table>
    
    <ui-table-decorator
        table-decorator
        [title]="{{tableTitle}}"
        [(columns)]="columns"
        (selectedColumnsChange)="onSelectedColumnsChange($event)">
    ></ui-table-decorator>
    
    <!--OTHER TABLE PARTS-->
    
</ui-table>
```

API
----------------------------------------------------------------

> **Selector**
>
> * `<ui-table-decorator>`
>
> **Input**
>
> * `title?: String` Title of table.
> * `columns?: ColumnDefinition[]` Column definition list.
> * `noColumnSelectedText?: String` Define text for column switcher when no columns selected. Default: Select column(s)'_
> * `columnsSelectedText?: String` Define text for column switcher when at least one column is selected. Default: ' column(s) selected'
>
> **Output**
>
> * `selectedColumnsChange?: any[]`: Define an `onSelectedColumnsChange(any[])` method that handles the column switcher's events.

### Typescript

```javascript
/**
 * @param name JSON property name
 * @param label title of the column header
 * @param width column's width
 * @param visible visibility
 * @param isDefault is the visibility changeable
 * @param resizable is the column resizable
 * @param sortable is the column sortable
 */
export class ColumnDefinition {

  constructor(private name: string,
              private label: string,
              private width: number,
              private visible: boolean,
              private isDefault: boolean,
              private resizable: boolean,
              private sortable: boolean) {
  }
}
```

#### Column definition list example

```javascript
this.columns = [
    new ColumnDefinition('firstName', 'First name', 300, true, true, true, true),
    new ColumnDefinition('lastName', 'Last name', 150, true, true, true, true),
    new ColumnDefinition('age', 'Age', 60, true, false, true, true),
    new ColumnDefinition('lastLoggedIn', 'Last logged in', 200, true, false, true, true),
];
```





Header group
================================================================

Header group encapsulates all the column headers into a single HTML tag.

Example
----------------------------------------------------------------

```
<ui-table>
    
    <ui-table-header-group
            table-header-group
    >
    	    <ui-table-header  
    	        [(width)]="column.width" 
    	        label="column.label"></ui-table-header>
    	    <ui-table-header  
                [(width)]="column.width" 
                label="column.label"></ui-table-header>
    </ui-table-header-group>
    
    ...
    

</ui-table>
```

API
----------------------------------------------------------------

> **Selector**
>
> * `<ui-table-header-group>`





Header
================================================================

Header component represents a column header.

Example
----------------------------------------------------------------

```html
<ui-table-header-group
    table-header-group>
    
    <template ngFor [ngForOf]="columns" let-column>
        <ui-table-header    
            *ngIf="column.visible"
            [label]="column.label"  
            [(width)]="column.width"    
            [resizable]="column.resizable"
            [sortable]="column.sortable"
        ></ui-table-header>
    </template>
    
</ui-table-header-group>
```

API
----------------------------------------------------------------

> **Selector**
>
> * `<ui-table-header>`
>
> **Input**
>
> * `label: string`: Label of header.
> * `width?: number`: Width of header. It also changes the width of associated column.
> * `sortable?: boolean`: Defines whether the sorting is applicable by the particular column.
> * `resizable?: boolean`: Defines whether the width of the column is resizable.
>
> **Output**
>
> * `widthChange?: number`: No method implementation required. Table columns automatically fit to header's width.
