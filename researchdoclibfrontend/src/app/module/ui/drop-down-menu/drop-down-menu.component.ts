import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
    selector: 'ui-drop-down',
    templateUrl: 'drop-down-menu.component.html'
})
export class DropDownMenuComponent implements OnInit {

    @Input() selected;
    @Input() items;

    @Output() dataEmitter: EventEmitter<any> = new EventEmitter();

    constructor() {
    }

    ngOnInit() {
    }

    onClick() {
        this.dataEmitter.emit(this.selected);
    }

}
