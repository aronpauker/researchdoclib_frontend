import { Component, Input } from '@angular/core';

@Component({
    selector: 'ui-form',
    templateUrl: 'form.component.html',
    styleUrls: ['form.component.scss']
})
export class FormComponent {

    @Input() label;
    @Input() error;

    constructor() {
    }

}
