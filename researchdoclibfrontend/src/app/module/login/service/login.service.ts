import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Response } from '@angular/http';
import { Authentication } from '../model/Authentication';
import { Observable } from 'rxjs';
import { LOGIN, LOGOUT } from './login.url';
import { HttpClientService } from '../../shared/service/http-client.service';

@Injectable()
export class LoginService {

    constructor(private http: Http, private httpClient: HttpClientService) {
    }

    /**
     * Apply form login.
     * @param authentication
     * @returns {Observable<R|T>}
     */
    login(authentication: Authentication): Observable<any> {
        let url = LOGIN;
        return this.http.post(url, JSON.stringify(authentication), {
            headers: new Headers({
                'Content-Type': 'text/plain',
                'X-Requested-With': 'XMLHttpRequest'
            })
        });
    }

    /**
     * Fetch details of user.
     * @returns {Observable<R>}
     */
    isLoggedIn(): Observable<any> {
        return this.httpClient.get('/api/users/self')
            .map(response => response);
    }

    /**
     * Logout.
     * @returns {Observable<R>}
     */
    logout(): Observable<any> {
        return this.httpClient.get(LOGOUT)
            .map(response => {
                return response;
            });
    }
}

