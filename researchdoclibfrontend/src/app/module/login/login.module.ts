import { NgModule } from '@angular/core';
import { LoginComponent } from './view/login.component';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { LoginRouterModule } from './login-router.module';
import { UiModule } from '../ui/ui.module';
import { LoginService } from './service/login.service';
import { SharedModule } from '../shared/shared.module';


@NgModule({
    imports: [
        CommonModule,
        HttpModule,
        FormsModule,
        UiModule,
        LoginRouterModule,
        SharedModule
    ],
    exports: [
        LoginRouterModule
    ],
    declarations: [
        LoginComponent
    ],
    providers: [
        LoginService
    ]
})
export class LoginModule {
}
