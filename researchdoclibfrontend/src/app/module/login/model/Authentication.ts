import { IsNotEmpty } from 'class-validator';
export class Authentication {
    @IsNotEmpty({message: 'Kötelezően kitöltendő'})
    username: string;

    @IsNotEmpty({message: 'Kötelezően kitöltendő'})
    password: string;
}
