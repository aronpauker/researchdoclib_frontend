import { Component, OnInit } from '@angular/core';
import { LoginService } from '../service/login.service';
import { Authentication } from '../model/Authentication';
import { Response } from '@angular/http';
import { EventHandlerService } from '../../shared/service/event-handler.service';
import { ToastNotification } from '../../shared/model/ToastNotification';
import { ErrorHandlerService } from '../../shared/service/error-handler.service';

@Component({
    selector: 'login',
    templateUrl: 'login.component.html',
    styleUrls: ['login.component.scss']
})
export class LoginComponent implements OnInit {
    labelUsername = 'Login to JIRA';

    auth: Authentication;
    lgn;
    required = true;
    errorMessage: any;

    constructor(private loginService: LoginService, private eventHandler: EventHandlerService, private errorHandler: ErrorHandlerService) {
    }

    ngOnInit() {
        this.auth = new Authentication();
        this.eventHandler.appNotification$.subscribe(
            error => {
                let notification = error;
                this.errorMessage = notification.message;
            });
    }


    // TODO - Back-end kétértelmű hibaüzenetet dob!!!
    login() {
        localStorage.clear();
        this.loginService.login(this.auth).subscribe(
            (response: Response) => {
                this.lgn = JSON.parse(response.text());
                localStorage.setItem('loggedIn', 'true');
                localStorage.setItem('token', this.lgn.token);
                this.eventHandler.emitLoginEvent();
            },
            (error) => {
                console.log(error.text());
                //  SHOULD BE REPAIRED IMMEDIATLY!!
                // if(error.text().indexOf('401') !== -1){
                //     this.errorHandler.showError('A felhasználó jóváhagyásra vár', 1500);
                // }
                this.errorHandler.handleError('login', error);
            }
        );
    }
}
