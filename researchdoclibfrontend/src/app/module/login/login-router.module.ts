import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './view/login.component';
import { SecurityService } from '../shared/service/security.service';


const routes: Routes = [
    {
        path: 'login',
        component: LoginComponent,
        canDeactivate: [SecurityService]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LoginRouterModule {
}
