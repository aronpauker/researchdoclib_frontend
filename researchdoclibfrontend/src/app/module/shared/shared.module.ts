import { NgModule } from '@angular/core';
import { ErrorHandlerService } from './service/error-handler.service';
import { EventHandlerService } from './service/event-handler.service';
import { HttpClientService } from './service/http-client.service';
import { SecurityService } from './service/security.service';
import { UserService } from './service/user.service';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { PageNotFoundComponent } from './page-not-found.component';


@NgModule({
    imports: [
        CommonModule,
        HttpModule,
        FormsModule
    ],
    exports: [
        // pipes
    ],
    declarations: [
        // pipes kell ide is
        PageNotFoundComponent
    ],
    providers: [
        ErrorHandlerService,
        EventHandlerService,
        HttpClientService,
        SecurityService,
        UserService
    ]
})
export class SharedModule {
}

