"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var error_handler_service_1 = require("./service/error-handler.service");
var event_handler_service_1 = require("./service/event-handler.service");
var http_client_service_1 = require("./service/http-client.service");
var security_service_1 = require("./service/security.service");
var user_service_1 = require("./service/user.service");
var common_1 = require("@angular/common");
var http_1 = require("@angular/http");
var forms_1 = require("@angular/forms");
var page_not_found_component_1 = require("./page-not-found.component");
var SharedModule = (function () {
    function SharedModule() {
    }
    return SharedModule;
}());
SharedModule = __decorate([
    core_1.NgModule({
        imports: [
            common_1.CommonModule,
            http_1.HttpModule,
            forms_1.FormsModule
        ],
        exports: [],
        declarations: [
            // pipes kell ide is
            page_not_found_component_1.PageNotFoundComponent
        ],
        providers: [
            error_handler_service_1.ErrorHandlerService,
            event_handler_service_1.EventHandlerService,
            http_client_service_1.HttpClientService,
            security_service_1.SecurityService,
            user_service_1.UserService
        ]
    })
], SharedModule);
exports.SharedModule = SharedModule;
