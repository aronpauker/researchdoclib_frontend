/**
 * Basic object for emitting a toast notification.
 */
export class ToastNotification {

  url: string;

  visible: boolean;

  message: string;

  duration: number;

  error: boolean;

  constructor(message?: string, duration?: number, error?: boolean, url?: string) {
    this.message = message || 'Unexpected error';
    this.duration = duration || 1500;
    this.error = error || false;
    this.url = url;
  }
}
