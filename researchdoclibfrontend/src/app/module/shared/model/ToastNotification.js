"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Basic object for emitting a toast notification.
 */
var ToastNotification = (function () {
    function ToastNotification(message, duration, error, url) {
        this.message = message || 'Unexpected error';
        this.duration = duration || 1500;
        this.error = error || false;
        this.url = url;
    }
    return ToastNotification;
}());
exports.ToastNotification = ToastNotification;
