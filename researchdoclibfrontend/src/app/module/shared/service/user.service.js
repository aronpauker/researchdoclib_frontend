"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var login_url_1 = require("../../login/service/login.url");
exports.SELF = login_url_1.BASEURL + 'user';
var UserService = (function () {
    function UserService(httpClient, router) {
        this.httpClient = httpClient;
        this.router = router;
    }
    UserService.prototype.getSelf = function () {
        var _this = this;
        var url = exports.SELF;
        return this.httpClient.get(url).catch(function (error) {
            _this.router.navigate(['login']);
            return rxjs_1.Observable.throw(error);
        });
    };
    UserService.prototype.isAdmin = function () {
        var isAdmin = localStorage.getItem('isAdmin');
        return isAdmin ? (isAdmin === 'true' ? true : false) : false;
    };
    UserService.prototype.isActive = function () {
        var isActive = localStorage.getItem('isActive');
        return isActive === 'true' ? true : false;
    };
    return UserService;
}());
UserService = __decorate([
    core_1.Injectable()
], UserService);
exports.UserService = UserService;
