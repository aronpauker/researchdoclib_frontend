"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var rxjs_1 = require("rxjs");
var HttpClientService = (function () {
    function HttpClientService(http, errorHandler) {
        this.http = http;
        this.errorHandler = errorHandler;
    }
    HttpClientService.prototype.buildHeaders = function () {
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('X-Authorization', 'Bearer ' + localStorage.getItem('token'));
        return headers;
    };
    HttpClientService.prototype.buildOptions = function (searchParam) {
        var options = { headers: this.buildHeaders() };
        if (searchParam) {
            options.params = searchParam;
        }
        return options;
    };
    HttpClientService.prototype.get = function (url, args) {
        var options = this.buildOptions(args);
        return this.http.get(url, options).map(function (response) {
            return response;
        }).catch(function (error) {
            return error;
        });
    };
    // .catch(error => {
    //     this.router.navigate(['login']);
    //     return Observable.throw(error);
    HttpClientService.prototype.post = function (url, data) {
        var options = this.buildOptions();
        return this.http.post(url, data, options).map(function (response) {
            return response;
        }).catch(function (error) {
            return error;
        });
    };
    HttpClientService.prototype.checkNetwork = function (fn) {
        var sessionTimeout = 600000;
        if (navigator.onLine) {
            var endOfTheSession = +localStorage.getItem('endOfTheSession');
            var now = new Date().getTime();
            localStorage.setItem('endOfTheSession', '' + (now + sessionTimeout));
            if (now < endOfTheSession) {
                return fn();
            }
            else {
                return rxjs_1.Observable.throw({ ok: false, status: 302, statusText: 'Redirected' });
            }
        }
        return rxjs_1.Observable.throw({ ok: false, status: 503, statusText: 'No Internet connection' });
    };
    return HttpClientService;
}());
HttpClientService = __decorate([
    core_1.Injectable()
], HttpClientService);
exports.HttpClientService = HttpClientService;
