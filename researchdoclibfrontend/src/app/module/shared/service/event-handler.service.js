"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Subject_1 = require("rxjs/Subject");
var EventHandlerService = (function () {
    function EventHandlerService(titleService, router) {
        this.titleService = titleService;
        this.router = router;
        this.loginEventSource = new Subject_1.Subject();
        this.appLoadingSource = new Subject_1.Subject();
        this.appNotificationSource = new Subject_1.Subject();
        this.loginEvent$ = this.loginEventSource.asObservable();
        this.appLoading$ = this.appLoadingSource.asObservable();
        this.appNotification$ = this.appNotificationSource.asObservable();
    }
    EventHandlerService.prototype.emitLoginEvent = function () {
        localStorage.setItem('endOfTheSession', '' + (new Date().getTime() + 60000));
        this.loginEventSource.next(true);
        this.router.navigateByUrl('home');
    };
    EventHandlerService.prototype.emitAlreadyLoggedIn = function () {
        localStorage.setItem('endOfTheSession', '' + (new Date().getTime() + 60000));
        this.loginEventSource.next(true);
    };
    EventHandlerService.prototype.setAppLoading = function (appLoading) {
        this.appLoadingSource.next(appLoading);
    };
    EventHandlerService.prototype.emitNotification = function (notification) {
        this.appNotificationSource.next(notification);
    };
    return EventHandlerService;
}());
EventHandlerService = __decorate([
    core_1.Injectable()
], EventHandlerService);
exports.EventHandlerService = EventHandlerService;
