import { Injectable } from '@angular/core';
import { HttpClientService } from './http-client.service';
import { Observable } from 'rxjs';
import { BASEURL } from '../../login/service/login.url';
import { Router } from '@angular/router';
export const SELF = BASEURL + 'user';

@Injectable()
export class UserService {

    constructor(private httpClient: HttpClientService, private router: Router) {
    }

    getSelf(): Observable<any> {
        let url = SELF;
        return this.httpClient.get(url).catch(error => {
            this.router.navigate(['login']);
            return Observable.throw(error);
        });
    }

    isAdmin(): boolean {
        let isAdmin = localStorage.getItem('isAdmin');
        return isAdmin ? (isAdmin === 'true' ? true : false) : false;
    }

    isActive(): boolean {
        let isActive = localStorage.getItem('isActive');
        return isActive === 'true' ? true : false;
    }
}
