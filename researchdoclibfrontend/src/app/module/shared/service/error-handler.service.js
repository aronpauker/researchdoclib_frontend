"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ToastNotification_1 = require("../model/ToastNotification");
var ErrorHandlerService = (function () {
    function ErrorHandlerService(eventHandler, router) {
        this.eventHandler = eventHandler;
        this.router = router;
    }
    ErrorHandlerService.prototype.handleError = function (source, error) {
        console.error('ErrorHandlerService.handleError()');
        console.error("source: " + source);
        console.error("status: " + error.status);
        console.error("statusText: " + error.statusText);
        console.error("causedBy:");
        console.error(error);
        if (error && error.status) {
            var errorStatus = +error.status;
            if (errorStatus < 300) {
                this.showError('A szerver nem válaszol', 1500);
            }
            else if (errorStatus >= 300 && errorStatus < 400) {
                return this.handle3xxRedirection(errorStatus);
            }
            else if (errorStatus >= 400 && errorStatus < 500) {
                return this.handle4xxClientError(source, errorStatus, error);
            }
            else if (errorStatus >= 500) {
                return this.handle5xxServerError(source, errorStatus, error);
            }
            else {
                this.showUnexpectedError();
            }
        }
        else {
            this.showUnexpectedError();
        }
    };
    ErrorHandlerService.prototype.handle3xxRedirection = function (errorStatus) {
        switch (errorStatus) {
            case 302:
                this.checkExpiresSession();
                this.router.navigateByUrl('/login');
                break;
            default:
                this.showUnexpectedError();
        }
    };
    /**
     * Handles client errors.
     * @param source
     * @param errorStatus
     * @param error
     * @returns {string}
     */
    ErrorHandlerService.prototype.handle4xxClientError = function (source, errorStatus, error) {
        if (source) {
            switch (errorStatus) {
                case 404:
                    if (error._body) {
                        if (source === 'login') {
                            this.showError('Sikertelen bejelentkezés', 1500);
                            return 'Hibás felhasználónév vagy jelszó';
                        }
                        else if (this.isRedirected(error)) {
                            this.checkExpiresSession();
                            this.router.navigateByUrl('/login');
                        }
                    }
                    else {
                        this.showUnexpectedError();
                    }
                    break;
                default:
                    this.showUnexpectedError();
            }
        }
    };
    /**
     * Handles server errors.
     * @param source
     * @param errorStatus
     * @param error
     * @returns {{email: string}}
     */
    ErrorHandlerService.prototype.handle5xxServerError = function (source, errorStatus, error) {
        switch (errorStatus) {
            case 500:
                if (error._body) {
                    return this.handle500ServerError(source, error);
                }
                else {
                    this.showUnexpectedError();
                }
                break;
            case 503:
                this.showError('Nincs Internetkapcsolat', 1500);
                break;
            default:
                this.showUnexpectedError();
        }
    };
    /**
     * Helper method to clear a form error object.
     * @param errorObject
     */
    ErrorHandlerService.prototype.clearErrorObject = function (errorObject) {
        Object.keys(errorObject).forEach(function (key) { return errorObject[key] = null; });
    };
    /**
     * Merges the client side validation errors into the error Object.
     * @param errors client side validation errors
     * @param errorObject form field error object
     * @returns {boolean}
     */
    ErrorHandlerService.prototype.isValid = function (errors, errorObject) {
        if (errors.length) {
            // client side validation error
            errors.forEach(function (error) { return errorObject[error.property] =
                error.constraints ?
                    error.constraints[Object.keys(error.constraints)[0]] :
                    error.children[0].constraints[Object.keys(error.children[0].constraints)[0]]; });
            // stop loading screen show notification
            this.eventHandler.setAppLoading(false);
            this.eventHandler.emitNotification(new ToastNotification_1.ToastNotification('Hibás vagy hiányzó adatok', 1000, true));
        }
        return !errors.length;
    };
    /**
     * Merges error response fields into errorObject
     * @param errorResponse
     * @param errorObject
     */
    ErrorHandlerService.prototype.renderErrors = function (errorResponse, errorObject) {
        if (errorResponse) {
            Object.keys(errorResponse).forEach(function (key) { return errorObject[key] = errorResponse[key]; });
        }
    };
    /**
     * Handles server errors with status code 500
     * @param source
     * @param error
     * @returns {{email: string}}
     */
    ErrorHandlerService.prototype.handle500ServerError = function (source, error) {
        var errorResponse = error.json();
        if (source === 'client') {
            if (errorResponse.exception.indexOf('DataIntegrityViolationException') >= 0) {
                var errorMessage = 'Az email már regisztrálva van';
                this.showError(errorMessage, 1500);
                return { email: errorMessage };
            }
            else {
                this.showUnexpectedError();
            }
        }
        if (errorResponse.exception.indexOf('HttpHostConnectException') >= 0) {
            this.showError('A szerver nem válaszol', 1500);
        }
        if (errorResponse.exception.indexOf('SocketTimeoutException') >= 0) {
            this.showError('Időtúllépés hiba', 1500);
        }
        else {
            this.showError('Rossz felhasználónév vagy jelszó', 1500);
        }
    };
    /**
     * Determines redirection. Spring redirect to login page.
     * @param error
     * @returns {boolean}
     */
    ErrorHandlerService.prototype.isRedirected = function (error) {
        if (error._body) {
            return error._body.indexOf('Cannot GET') > 0;
        }
        return false;
    };
    /**
     * Checks if there was a session according to local storage.
     */
    ErrorHandlerService.prototype.checkExpiresSession = function () {
        var wasLoggedIn = localStorage.getItem('loggedIn') === 'true';
        if (wasLoggedIn) {
            this.showError('Az ön munkamenete lejárt', 2000);
        }
    };
    /**
     * Shows a simple unexpected error.
     */
    ErrorHandlerService.prototype.showUnexpectedError = function () {
        this.showError('Váratlan hiba történt', 1500);
    };
    /**
     * Shows an error message.
     * @param message
     * @param duration
     */
    ErrorHandlerService.prototype.showError = function (message, duration) {
        this.eventHandler.setAppLoading(false);
        this.eventHandler.emitNotification(new ToastNotification_1.ToastNotification(message, duration, true));
    };
    return ErrorHandlerService;
}());
ErrorHandlerService = __decorate([
    core_1.Injectable()
], ErrorHandlerService);
exports.ErrorHandlerService = ErrorHandlerService;
