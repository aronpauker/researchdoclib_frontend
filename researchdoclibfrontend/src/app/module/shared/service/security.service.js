"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ToastNotification_1 = require("../model/ToastNotification");
var SecurityService = (function () {
    function SecurityService(userService, eventHandler, router) {
        this.userService = userService;
        this.eventHandler = eventHandler;
        this.router = router;
    }
    SecurityService.prototype.ngOnInit = function () {
        this.userService.getSelf().subscribe(function (response) {
            console.log(response);
        });
    };
    SecurityService.prototype.canActivate = function () {
        return this.isLoggedIn();
    };
    SecurityService.prototype.canActivateChild = function () {
        return this.isLoggedIn();
    };
    SecurityService.prototype.canDeactivate = function (component, currentRoute, currentState, nextState) {
        var isLoggedIn = this.isLoggedIn();
        if (component || currentRoute || currentState || nextState) {
        }
        var isOnline = navigator.onLine;
        if (!isOnline) {
            this.eventHandler.emitNotification(new ToastNotification_1.ToastNotification('Nincs Internetkapcsolat', 1500, true));
        }
        else {
            return isLoggedIn;
        }
    };
    SecurityService.prototype.isLoggedIn = function () {
        var loggedIn = localStorage.getItem('loggedIn') === 'true';
        if (!loggedIn) {
            this.router.navigate(['/login']);
        }
        return loggedIn;
    };
    return SecurityService;
}());
SecurityService = __decorate([
    core_1.Injectable()
], SecurityService);
exports.SecurityService = SecurityService;
