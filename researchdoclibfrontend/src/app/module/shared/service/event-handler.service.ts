import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';

@Injectable()
export class EventHandlerService {

    private loginEventSource = new Subject<boolean>();
    private appLoadingSource = new Subject<boolean>();
    private appNotificationSource = new Subject<any>();

    loginEvent$ = this.loginEventSource.asObservable();
    appLoading$ = this.appLoadingSource.asObservable();
    appNotification$ = this.appNotificationSource.asObservable();

    constructor(private titleService: Title, private router: Router) {
    }

    emitLoginEvent() {
        localStorage.setItem('endOfTheSession', '' + (new Date().getTime() + 60000));
        this.loginEventSource.next(true);
        this.router.navigateByUrl('home');
    }

    emitAlreadyLoggedIn() {
        localStorage.setItem('endOfTheSession', '' + (new Date().getTime() + 60000));
        this.loginEventSource.next(true);
    }

    setAppLoading(appLoading: boolean) {
        this.appLoadingSource.next(appLoading);
    }

    emitNotification(notification) {
        this.appNotificationSource.next(notification);
    }
}
