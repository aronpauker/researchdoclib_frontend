import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptionsArgs } from '@angular/http';
import { Observable } from 'rxjs';
import { ErrorHandlerService } from './error-handler.service';

@Injectable()
export class HttpClientService {

    constructor(private http: Http, private errorHandler: ErrorHandlerService) {
    }

    buildHeaders(): Headers {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('X-Authorization', 'Bearer ' + localStorage.getItem('token'));
        return headers;
    }

    buildOptions(searchParam?): RequestOptionsArgs {
        let options: RequestOptionsArgs = {headers: this.buildHeaders()};
        if (searchParam) {
            options.params = searchParam;
        }
        return options;
    }

    get(url: string, args?: any): Observable<any> {
        let options = this.buildOptions(args);
        return this.http.get(url, options).map(response => {
            return response;
        }).catch(error => {
            return error;
        });
    }

// .catch(error => {
//     this.router.navigate(['login']);
//     return Observable.throw(error);

    post(url: string, data: any): Observable<Response> {
        let options = this.buildOptions();
        return this.http.post(url, data, options).map(response => {
            return response;
        })
    }

    checkNetwork(fn): Observable<any> {
        let sessionTimeout = 600000;

        if (navigator.onLine) {
            let endOfTheSession = +localStorage.getItem('endOfTheSession');
            let now = new Date().getTime();

            localStorage.setItem('endOfTheSession', '' + (now + sessionTimeout));
            if (now < endOfTheSession) {
                return fn();
            } else {
                return Observable.throw({ok: false, status: 302, statusText: 'Redirected'});
            }
        }
        return Observable.throw({ok: false, status: 503, statusText: 'No Internet connection'});
    }
}
