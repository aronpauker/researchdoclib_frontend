import { Injectable, OnInit } from '@angular/core';
import { EventHandlerService } from './event-handler.service';
import {
    ActivatedRouteSnapshot,
    CanActivate,
    CanActivateChild,
    CanDeactivate,
    Router,
    RouterStateSnapshot
} from '@angular/router';
import { UserService } from './user.service';
import { ToastNotification } from '../model/ToastNotification';

@Injectable()
export class SecurityService implements OnInit, CanActivate, CanActivateChild, CanDeactivate<any> {

    constructor(private userService: UserService,
                private eventHandler: EventHandlerService,
                private router: Router) {
    }

    ngOnInit(): void {
        this.userService.getSelf().subscribe(
            response => {
                console.log(response);
            }
        );
    }

    canActivate(): boolean {
        return this.isLoggedIn();
    }

    canActivateChild(): boolean {
        return this.isLoggedIn();
    }

    canDeactivate(component: any, currentRoute: ActivatedRouteSnapshot,
                  currentState: RouterStateSnapshot, nextState?: RouterStateSnapshot): boolean {
        let isLoggedIn = this.isLoggedIn();
        if (component || currentRoute || currentState || nextState) {

        }
        let isOnline = navigator.onLine;
        if (!isOnline) {
            this.eventHandler.emitNotification(new ToastNotification('Nincs Internetkapcsolat', 1500, true));
        } else {
            return isLoggedIn;
        }
    }

    isLoggedIn() {
        let loggedIn = localStorage.getItem('loggedIn') === 'true';
        if (!loggedIn) {
            this.router.navigate(['/login']);
        }
        return loggedIn;
    }
}
