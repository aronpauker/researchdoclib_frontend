import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NglModule } from 'ng-lightning';
import { AppComponent } from './view/app.component';
import { AppRouterModule } from "./app-router.module";
import { UiModule } from "../module/ui/ui.module";
import { LoginModule } from "../module/login/login.module";
import { HttpModule } from "@angular/http";
import { FormsModule } from "@angular/forms";

@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    NglModule.forRoot(),

    UiModule,
    LoginModule,

    AppRouterModule
  ],
  declarations: [
    AppComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
